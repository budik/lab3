
// console.log('123 MEMO');


const data_array= Array.from({length:100000},() => Math.floor(Math.random() * 100));

console.log('[data_array]',data_array);

const memoize = func => {

    const result = {};
    return (...args) => {
        const key = JSON.stringify(args);
        if (result[key]) {
            return result[key];
        }
        return result[key] = func(...args);
    }

}
    const squareData = memoize(arr => arr.map( item => item * item));

    console.time('1');

    let res = squareData(data_array);

    console.timeEnd('1');

    console.log('res1',res);


