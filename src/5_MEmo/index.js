import './native_demo';
import React,{Component} from 'react';

const compareData = (prevProps,nextProps) => {
    console.log('[Compare]',prevProps,nextProps);
    return prevProps.name === nextProps.name;
}

const FnMemo = React.memo(({name}) => {
    console.log('Render child');
    return(
        <>
            <h1>NAme:{name}</h1>
        </>
    )
},compareData
)


class Parent extends Component{

    state ={
        name:"Dexter"

    }


componentDidMount = () => {
        this.timer = setInterval(() => {

            this.setState({name : "Dexter"})

    },500);
}

changeData = () => {
        this.setState({name:"Debra"});
}

    render = () => {
        console.log('Render Parent');
        const {name} = this.state;
        const {changeData} = this;
        return(
            <div>
                <h1>MEmo PArent</h1>
                <button onClick={changeData}>Change !</button>

                <FnMemo name={name}/>
            </div>
        );
    }

}

export default Parent;