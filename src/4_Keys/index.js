import React,{Component} from 'react';
import uniqid from 'uniqid';


const basarray = Array.from({length:10}, ()=> (
    {
        id:uniqid('item-'),
        name :Math.floor(Math.random() * 100)
    }
))



class DataContainer extends Component{

    state = {
        data:basarray,
        title:''
    }

    changeHandler = e => {
        this.setState({title: e.target.value})
    }

    addItem = (position) => _ => {
const {title,data} = this.state;
        let newItem = {
            id:uniqid('item-'),
            name:title
        };

        let convertedData;
        if(position ==='start'){
            convertedData =[
    newItem,
    ...data
];
        }
        else{
            convertedData = [...data,newItem];

        }
this.setState({data:convertedData});
    }

    removedItem = (id) =>_=> {
        const raw = [...this.state.data];
        const removedData = raw.filter( item => item.id !== id);

        this.setState({data:removedData})
}

    render = () =>  {
        const {data,title} = this.state;
        const {changeHandler,addItem,removedItem} = this;
        return (
            <div>
                <div>
                <h1> KEys Demo</h1>
                <input type="text" value={title} onChange={changeHandler}/>
                <button onClick={addItem('start')}>Add Start</button>
                <button onClick={addItem('end')}>Add end</button>
                </div>
<div>
    <ul>
        {
            data.map((item,index) => {
                return <ListItem
                    key={item.id}
                    item={item}
                    remove={removedItem}/>
        })
        }
    </ul>
</div>

            </div>
        );
    }
}


const ListItem = React.memo(({item,remove}) => {
    console.log('Item Rendered');
    return (
        <li>
            {item.name}
            <button onClick={remove(item.id)}>-</button>
        </li>
    )

}
)
export default DataContainer;