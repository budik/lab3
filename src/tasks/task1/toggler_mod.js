import React from 'react';
import PropTypes from 'prop-types';
// import ComponentWithPropTypes from "../../1_Proptypes";

export const Toggler_mod  = ({children,active,action,label,gender,layout}) => {

    return(
        <div className="toggler">
            <b>{ label }</b>
            { React.Children.count(children) > 0 && (
                <>{
                    React.Children.map(children, (chilItem) => {
                        if (React.isValidElement(chilItem)) {
                            return React.cloneElement(chilItem,
                                {
                                    active: chilItem.props.value === active,
                                    action: action(chilItem.props.value),
                                    gender: gender,
                                    layout:chilItem.props.layout

                                }
                            );
                        }
                    })
                }
                </>
            )
            }


        </div>
    )
}

export const TogglerItem_mod = ({children,active,action,value,gender}) => {
// console.log(active,action,value);
    return(
        <button
            type="button"
            className={active ? 'toggler__item active' : 'toggler__item'}
            onClick={action}
            style={gender==='male' ? {color:'#0043ff',display:value} : {color:'#cc00ff',display:value}
            }


        >{children} {gender} {value}</button>
    )
}

//
Toggler_mod.propTypes = {

    name:PropTypes.string.isRequired
    ,action:PropTypes.func
    ,children:PropTypes.instanceOf(TogglerItem_mod)
    ,activeState:PropTypes.string

}