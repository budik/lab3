import React,{Component} from 'react';
import {Toggler,TogglerItem} from "../../2_AdvancedChilds/toggler";
// import PropTypes from 'prop-types';

import PropTypes from 'prop-types';
class ControlledForm_mod extends Component{

    state = {
        first:'',
        last:'',
        align:'left',
        task:''
    }

    changeToggler = value => _ => {
        this.setState({align:value});
    }

    changeHandler = e => {
        let name = e.target.name;
        let value = e.target.value;


        this.setState({
            [name]:value
        });
    }


    onSubmit = (e) => {
        e.preventDefault();
        console.log('[state]',this.state);
    }

    render = () => {
        const {align,first,last,task} = this.state;
        const {changeToggler,changeHandler,onSubmit} = this;
        const {placeholder,type,contentLength,contentMaxLength} = this.props;
        return (
            <form className="form" onSubmit={onSubmit}>
                <h1>Controlled Form</h1>
                <label>
                    <div> First</div>
                    <input type="text"  name="first" value={first} onChange={changeHandler}/>
                </label>
                <label>
                    <div> Last</div>
                    <input type="text" name="last" value={last} onChange={changeHandler}/>
                </label>
                <label>
                    <div> TASK2</div>
                    <input
                        name="task"
                        type={type}
                           placeholder={placeholder}
                           value={task}
                           onChange={changeHandler}
                        maxLength={contentLength && contentMaxLength }

                    />
                </label>
                <button> SEND </button>
            </form>
        )
    }

}
//
ControlledForm_mod.propTypes = {
    type:PropTypes.oneOf(['number','password','text']).isRequired,
    placeholder: PropTypes.string,
    value:  PropTypes.oneOfType([ PropTypes.string,PropTypes.any]),
    contentLength: PropTypes.bool,
    contentMaxLength: PropTypes.number,
    handler:PropTypes.func.isRequired
}

export default ControlledForm_mod;