import React,{Component} from 'react';
// import {Toggler,TogglerItem} from "../../2_AdvancedChilds/toggler";
// import PropTypes from 'prop-types';
// import {TogglerItem_mod,Toggler_mod} from "../task1/toggler_mod";

import PropTypes from 'prop-types';
class Input extends Component{

    state = {
        name:'',
        password:'',
        align:'left',
        age:''
    }

    changeToggler = value => _ => {
        this.setState({align:value});
    }

    changeHandler = e => {
        let name = e.target.name;
        let value = e.target.value;


        this.setState({
            [name]:value
        });
    }


    onSubmit = (e) => {
        e.preventDefault();
        console.log('[state]',this.state);
    }

    render = () => {
        const {align,first,last,task} = this.state;
        const {changeToggler,changeHandler,onSubmit} = this;
        const {placeholder,type,contentLength,contentMaxLength,name} = this.props;
        return (
                <label>
                    <div> {name}</div>
                    <input
                        name={name}
                        type={type}
                        placeholder={placeholder}
                        value={task}
                        onChange={changeHandler}
                        maxLength={contentLength && contentMaxLength }

                    />
                </label>
        )
    }

}
//
Input.propTypes = {
    type:PropTypes.oneOf(['number','password','text']).isRequired,
    placeholder: PropTypes.string,
    value:  PropTypes.oneOfType([ PropTypes.string,PropTypes.any]),
    contentLength: PropTypes.bool,
    contentMaxLength: PropTypes.number,
    handler:PropTypes.func.isRequired
}

export default Input;