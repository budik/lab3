import React,{Component} from 'react';

// import logo from './logo.svg';
// import './App.css';

// 1_Proptypes
// import ComponentWithPropTypes from "./1_Proptypes";

//2 ADvanced Childs
// import DemoChild from "./2_AdvancedChilds";
import {Toggler_mod,TogglerItem_mod} from "./tasks/task1/toggler_mod";
// import {Toggler,TogglerItem} from "./2_AdvancedChilds/toggler";

//3 Controlled Form
// import ControlledForm from "./3_Controlled_form";

// 4 KEys
// import Keys from './4_Keys';

//5 MEmo
// import Memo from "./5_MEmo";

// import ControlledForm_mod from "./tasks/task3/index";
import Input from "./tasks/task3/index";

const Message = ({text}) => (<span>{text}</span>);

class App extends Component{

    state = {
        align:'left',
        gender:'male',
        layout: 'right',
        name :''
    }


    changeToggler = value => _ => {
        if (this.state.gender === 'male'){
            // console.log('[gender]',this.state.gender);
            this.setState({gender:'female'});
        }
        else{
            // console.log('[gender]',this.state.gender);
            this.setState({gender:'male'});
        }
        // this.setState({align:value,layout:value});
        // console.log('[gender]',this.state.gender);
    }


    render = () => {
const{align,gender,layout,name} = this.state;
        return (
            <div className="App">
                {/***********************/}
                {/*       1_Proptypes   */}
                {/***********************/}

                {/*<ComponentWithPropTypes
stringProp="asdasd"
arrayProp={[1,2,3,4]}
somethingTrue={true}
action={ () => {console.log(123)} }
type="Bar"
element={<Message/>}
user={{
name:"123",
    count:123
}}
someStuff={123123}
>
<Message/>
    </ComponentWithPropTypes>*/}

                {/*******************/}
                {/*2 Advanced Childs*/}
                {/********************/}
                {/*<DemoChild>*/}
                {/*    <Message/>*/}
                {/*    <Message/>*/}
                {/*    <Message/>*/}
                {/*    {null}*/}
                {/*    {false}*/}
                {/*    {10+20}*/}
                {/*</DemoChild>*/}

                {/*****************/}
                {/*3ControlledForm*/}
                {/*****************/}
                {/*<ControlledForm/>*/}

                {/*****************/}
                {/*4 KEys*/}
                {/*****************/}
                {/*<Keys/>*/}
                {/*<Memo/>*/}

                {/*****************/}
                {/*TASK 1*/}
                {/*****************/}

                {/*<Toggler_mod active={align} action={this.changeToggler} label="Align" gender={gender} name="toggler">*/}
                {/*    <TogglerItem_mod  value="left">LEft</TogglerItem_mod>*/}
                {/*    <TogglerItem_mod  value="centr">Center</TogglerItem_mod>*/}
                {/*    <TogglerItem_mod  value="right">Right</TogglerItem_mod>*/}
                {/*</Toggler_mod>*/}
                {/*<Toggler_mod active={align} action={this.changeToggler} label="Layout" gender={gender} name="shmogler">*/}
                {/*    <TogglerItem_mod  value="left">LEft</TogglerItem_mod>*/}
                {/*    <TogglerItem_mod  value="center">Center</TogglerItem_mod>*/}
                {/*    <TogglerItem_mod  value="right">Right</TogglerItem_mod>*/}
                {/*    <TogglerItem_mod  value="baseline"></TogglerItem_mod>*/}
                {/*</Toggler_mod>*/}

{/*                /!*****************!/*/}
{/*                /!*TASK 2*!/*/}
{/*                /!*****************!/*/}
{/*<ControlledForm_mod*/}
{/*    placeholder = '27/11/2020'*/}
{/*    type="password"*/}
{/*    contentLength={true}*/}
{/*    contentMaxLength={6}*/}
{/*    handler={this.changeToggler}*/}
{/*/>*/}

                {/*****************/}
                {/*TASK 3*/}
                {/*****************/}
                <Input
                    name="Name"
                    placeholder = "What's your name?"
                    type="text"
                    contentLength={true}
                    contentMaxLength={6}
                    handler={this.changeToggler}
                />

                <Input
                    name="Password"
                    placeholder = "What's you password"
                    type="password"
                    contentLength={true}
                    contentMaxLength={6}
                    handler={this.changeToggler}
                />

                <Toggler_mod active={align} action={this.changeToggler} label="Gender" gender={gender} name="shmogler">
                    <TogglerItem_mod  value="male">M</TogglerItem_mod>
                    <TogglerItem_mod  value="female">ZH</TogglerItem_mod>
                </Toggler_mod>
                <Input
                    name="Age"
                    placeholder = "18 already?"
                    type="number"
                    contentLength={true}
                    contentMaxLength={6}
                    handler={this.changeToggler}
                />
                <Toggler_mod active={align} action={this.changeToggler} label="Layout" gender={gender} name="shmogler">
                    <TogglerItem_mod  value="left">LEft</TogglerItem_mod>
                    <TogglerItem_mod  value="center">Center</TogglerItem_mod>
                    <TogglerItem_mod  value="right">Right</TogglerItem_mod>
                    <TogglerItem_mod  value="baseline"></TogglerItem_mod>
            </Toggler_mod>
                <Input
                    name="Lang"
                    placeholder = "Do you even speak ?"
                    type="text"
                    contentLength={true}
                    contentMaxLength={6}
                    handler={this.changeToggler}
                />
            </div>
        );
        }

}




export default App;
