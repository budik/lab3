import React,{Component} from 'react';
import {Toggler,TogglerItem} from "../2_AdvancedChilds/toggler";

class ControlledForm extends Component{

    state = {
        first:'',
        last:'',
        align:'left'
    }

    changeToggler = value => _ => {
        this.setState({align:value});
    }

    changeHandler = e => {
        let name = e.target.name;
        let value = e.target.value;


        this.setState({
            [name]:value
        });
    }


    onSubmit = (e) => {
        e.preventDefault();
        console.log('[state]',this.state);
    }

    render = () => {
        const {align,first,last} = this.state;
        const {changeToggler,changeHandler,onSubmit} = this;
        return (
            <form className="form" onSubmit={onSubmit}>
                <h1>Controlled Form</h1>
                <label>
                    <div> First</div>
                    <input type="text"  name="first" value={first} onChange={changeHandler}/>
                </label>
               <label>
                    <div> Last</div>
                    <input type="text" name="last" value={last} onChange={changeHandler}/>
                </label>
                <Toggler active={align} action={changeToggler} label="Align">
                    <TogglerItem  value="left">LEft</TogglerItem>
                    <TogglerItem  value="centr">Center</TogglerItem>
                    <TogglerItem  value="right">Right</TogglerItem>
                </Toggler>
                <button> SEND </button>
            </form>
        )
    }

}

export default ControlledForm