import React from 'react';
import PropTypes from 'prop-types';

const ComponentWithPropTypes = (props) => {

    return(
        <div>
            <h1> PROPTYPES</h1>
            {props.children}
        </div>
    )


}

ComponentWithPropTypes.propTypes = {
    children: PropTypes.element.isRequired,
    stringProp: PropTypes.string,
    arrayProp: PropTypes.array,
    somethingTrue: PropTypes.bool,
    action:PropTypes.func,
    type:PropTypes.oneOf([
        "Foo","Bar"
    ]).isRequired,
    element:PropTypes.element,
    user: PropTypes.shape({
        name:PropTypes.string.isRequired,
        count:PropTypes.number
    }),
    someStuff:PropTypes.any.isRequired

}

export default ComponentWithPropTypes;