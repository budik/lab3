import React from 'react';


export const Toggler  = ({children,active,action,label}) => {

    return(
        <div className="toggler">
            <b>{ label }</b>
            { React.Children.count(children) > 0 && (
                <>{
                    React.Children.map(children, (chilItem) => {
                        if (React.isValidElement(chilItem)) {
                            return React.cloneElement(chilItem,
                                {
                                   active: chilItem.props.value === active,
                                    action: action(chilItem.props.value)
                                }
                            );
                        }
                    })
                  }
                  </>
            )
            }


        </div>
    )
}

export const TogglerItem = ({children,active,action,value}) => {
// console.log(active,action,value);
    return(
        <button
            type="button"
            className={active ? 'toggler__item active' : 'toggler__item'}
        onClick={action}
        >{children}</button>
    )
}