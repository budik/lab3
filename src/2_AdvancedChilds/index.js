import React from 'react';




const DemoChild = ({children}) => {
    console.log('[counter]',React.Children.count(children));
    // console.log('[only?]',React.Children.only(children));
    return(
        <div>
            <h1>DEMO CHILDS</h1>
            {
                React.Children.count(children) >= 0 && (
                    <>{
                        React.Children.map(children, (chilItem) => {
                            console.log('[child]',chilItem, React.isValidElement(chilItem));
                            if (React.isValidElement(chilItem)) {
                                return React.cloneElement(chilItem,
                                    {
                                        text:'123'
                                    }
                                );
                            }
                        })
                    }
                    </>

                )

            }
        </div>

    )

}

export default DemoChild;